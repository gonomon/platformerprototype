﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


public class RankingofPlayers : MonoBehaviour
{
    public GameObject mainPlayer;
    public GameObject opponentList;
    public int rankNow = 1;
    [SerializeField] private GameObject finishLine;

    // Update is called once per frame
    void LateUpdate()
    {
        float mainPlayerPosZ = mainPlayer.transform.position.z;
        if (mainPlayerPosZ <= finishLine.transform.position.z-0.2f)
        {
            {
                // Create array of positions then reverse sort them for showing them in correct order
                Transform tran = opponentList.transform;
                var playerPositions = new List<float>{ mainPlayerPosZ, tran.GetChild(0).position.z, tran.GetChild(1).position.z,
                tran.GetChild(2).position.z, tran.GetChild(3).position.z,tran.GetChild(4).position.z,tran.GetChild(5).position.z,
                tran.GetChild(6).position.z,tran.GetChild(7).position.z,tran.GetChild(8).position.z,tran.GetChild(9).position.z};
                playerPositions.Sort();
                playerPositions.Reverse();
                // check which position the main player is in, then write it to rankNow.
                for (int i = 0; i < 11; i++)
                {
                    if (mainPlayerPosZ == playerPositions[i]) rankNow = i + 1;
                }
                Debug.Log("Rank" + rankNow);
                GetComponent<TextMeshProUGUI>().text = "Rank:" + rankNow;
            }
        }
    }
}
