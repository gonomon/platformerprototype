﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZRotate : MonoBehaviour
{
    [SerializeField] private float zRotate;
    void Update()
    {
        // Rotate in z
        transform.Rotate(Vector3.forward * zRotate);
    }
}
