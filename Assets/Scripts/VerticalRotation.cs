﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VerticalRotation : MonoBehaviour
{
    [SerializeField] private float xRotate;
    // Update is called once per frame
    void Update()
    {
        transform.Rotate(xRotate, 0f, 0f, Space.Self);
    }
}
