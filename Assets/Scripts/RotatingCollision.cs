﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatingCollision : MonoBehaviour
{
    // Rotation makes player go to sides, amount of movement cons.
    [SerializeField] private Vector3 movementConstant;
    private void OnTriggerEnter(Collider other)
    {
        //if player is on rotationg platform, move the player.
        other.transform.position = Vector3.Lerp(other.transform.position, other.transform.position + movementConstant, 1f);
        //other.attachedRigidbody.MovePosition(movementConstant);
    }
}
