﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionRestart : MonoBehaviour
{
    private Vector3 startState;
    private void Start()
    {
        // note initial position
        startState = transform.position;
    }
    private void Update()
    {
        //if player out of bounds, restart at start position
        if(transform.position.y < -15 || transform.position.y > -4)
        {
            transform.position = startState;
        }
    }
    private void OnTriggerEnter(Collider other)
    {

        //if player collides with obstacle, restart at start position, and only with objects on first and second half.
        if (transform.position.z < 100 || (transform.position.z > 150 && transform.position.z < 230))
        {
            transform.position = startState;
        }
    }
}
