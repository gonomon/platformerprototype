﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    private float MouseStateX;
    private float MoveAmount;
    [SerializeField] private float SwerveSpeed;
    private float SwerveMovement;
    [SerializeField] private float ConstantSpeedZ;
    [SerializeField] private GameObject finishLine;
    public GameObject cameraPos;
    [SerializeField] private GameObject wallPosition;

    void Update()
    {
        // Here if player clicks somewhere and moves their mouse, character will follow in the direction and amount.
        // If player does not click and move the mouse the movement will not be affected in anyways
        // So we find the movement amount with positionOfClick - positionOfNow.
        if (transform.position.z <= finishLine.transform.position.z)
        {
            if (Input.GetMouseButtonDown(0))
            {
                MouseStateX = Input.mousePosition.x;
            }
            else if (Input.GetMouseButton(0))
            {
                MoveAmount = Input.mousePosition.x - MouseStateX;
                MouseStateX = Input.mousePosition.x;
            }
            else if (Input.GetMouseButtonUp(0))
            {
                MoveAmount = 0;
            }
            // Can add some extra speed with SwerveSpeed constant
            SwerveMovement = Time.deltaTime * SwerveSpeed * MoveAmount;
            // Also apply constant Z motion
            transform.Translate(SwerveMovement, 0, ConstantSpeedZ * Time.deltaTime);
        }
        //if game is finish, and camera is on wall, stop char motion
        else if(cameraPos.transform.position.z >= (wallPosition.transform.position.z - 0.2f))
        {
            if (GetComponent<Animator>().enabled == true) GetComponent<Animator>().enabled = false;
            if (GetComponent<CollisionRestart>().enabled == true) GetComponent<CollisionRestart>().enabled = false;
        }
    }
}
