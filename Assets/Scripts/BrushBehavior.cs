﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrushBehavior : MonoBehaviour
{
    //public GameObject brushObject;
    public GameObject cameraObject;
    //[SerializeField]private float brushSize;
    public GameObject percentageShow;

    /* void Update()
     {
         if(cameraObject.transform.position.z > 254f)
         {
             if (Input.GetMouseButton(0))
             {
                 var Ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                 RaycastHit rayHit;
                 if(Physics.Raycast(Ray, out rayHit))
                 {
                     var goToPoint = Instantiate(brushObject, rayHit.point + Vector3.up * 0.1f + Vector3.back * 100f, Quaternion.identity, transform);
                     goToPoint.transform.localScale = Vector3.one * brushSize;

                 }
             }
         }
     }
 */
    void Update()
    {
        // if camera object is in position of drawing the wall,
        if (cameraObject.transform.position.z > 254f)
        {
            // Start the drawing tools by activating them, also open percentage show window
            percentageShow.SetActive(true);
            GetComponent<PaintIn3D.P3dHitScreen>().enabled = true;
        }
    }
}