﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HorizontalRotate : MonoBehaviour
{
    [SerializeField] private float yRotate;
    void Update()
    {
        // Rotate in y, for horizontal obstacle
        transform.Rotate(0f, yRotate, 0f, Space.Self);
    }
}
