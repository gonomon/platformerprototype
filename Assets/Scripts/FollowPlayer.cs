﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour
{
    public GameObject mainPlayer;
    [SerializeField]private Vector3 cameraOffset = new Vector3(0f, 0f, 0f);
    private float cameraUpdateRate = 1f;
    private float cameraToWallRate = 0.05f;
    [SerializeField] private GameObject wallPosition;
    [SerializeField] private GameObject finishLine;

    // Update is called once per frame
    void LateUpdate()
    {
        Vector3 cameraPosition = transform.position;
        // chase player before finish line
        if (mainPlayer.transform.position.z <= finishLine.transform.position.z)
        {
            Vector3 toPosition = mainPlayer.transform.position + cameraOffset;
            //transform.position = toPosition; //follow player with an offset

            //Smoothly translates between current position to new position with update rate
            transform.position = Vector3.Lerp(cameraPosition, toPosition, cameraUpdateRate);
        }
        // After finish line go to wall for drawing, if player drops at the moment, dont follow.
        else if (mainPlayer.transform.position.y > -6f)
        {
            transform.position = Vector3.Lerp(cameraPosition, wallPosition.transform.position, cameraToWallRate);
        }
    }
}
